#include "circular_buffer.h"

#include <algorithm>
#include <vector>
#include <string>
#include <numeric>
#include <cmath>
#include <iostream>
namespace hid {

namespace fir {

/// Types of FIR filter supported
enum class FIRFilterType{kUniform = 0, kHarmonic};

/// FIR Filter with max number of taps set at compile time
/// Can be of type `FIRFilterType:kUniform` or `FIRFilterType::kHarmonic`
/// Uses time-domain convolution and hence will be slow for higher length signals/filter
template <typename T, std::size_t N>
class FIRFilter {
    static_assert(N > 0, "Number of taps cannot be 0");
  public:
    using DataBuffer = cb::ContiguousBlockCircularBuffer<T, N>;
    using FilterCoefs = std::vector<T>;

    /// Deleted default constructor. Filter needs to be initialized with the filter type. 
    FIRFilter() = delete;

    /// Creates a filter with coefficients initialized based on the filter type.
    FIRFilter(const FIRFilterType filter_type, std::size_t num_taps = N) {
        assert(num_taps <= N);
        num_filter_taps_ = num_taps;

        // Reserving memory 
        coefs_.reserve(num_filter_taps_);
        mac_buffer_.reserve(num_filter_taps_);
        buf_ = DataBuffer(num_filter_taps_);
        mac_buffer_ = std::vector<T>(num_filter_taps_, T());

        switch (filter_type) {
            case FIRFilterType::kUniform: {
                coefs_ = FilterCoefs(num_filter_taps_, (1.0/num_filter_taps_)); 
                filter_type_ = "uniform";
                break;
            }
            case FIRFilterType::kHarmonic: {
                coefs_ = FilterCoefs(num_filter_taps_, T());
                const float denom = num_filter_taps_ * (num_filter_taps_ + 1)/2.0;
                for (std::size_t i = num_filter_taps_; i >= 1; i--) {
                    coefs_[i-1] = i/denom;
                }
                filter_type_ = "harmonic";
                break;
            }
            default:
                // just return?
                assert(false);
        }

        initialized_ = true;
    }

    /// Adds a new data point to the data buffer and returns the output based on this 
    /// and the past `N - 1` points
    T add_data_point(const T& data) {
        if (!is_initialized()) {
            // add print/log?
            return T();
        }
        buf_.push(data);
        return multiply_and_accumulate();
    }

    /// Signal should be a container with size() API and [] operator defined and ideally must be of type T.
    /// Signal should also have a constructor defined to create a signal of arbitrary length and initialized with a
    /// default value. E.g ```Signal signal(4, 1);``` should yield - {1,1,1,1}
    /// NOTE: This function computes the `full` convolution output. Equivalent to numpy.convolve(signal, filter, 'full')
    template <typename Signal>
    Signal filter(const Signal& signal, double* input_power = nullptr, double* output_power = nullptr) {

        const std::size_t input_length = signal.size();
        
        if (input_length == 0 || !is_initialized()) {
            return Signal();
        }
        
        const std::size_t output_length = input_length + num_filter_taps_ - 1;
        Signal out_signal(output_length, T());
        for (std::size_t i = 0; i < output_length; i++) {
            buf_.push( i < input_length? signal[i] : T() );
            out_signal[i] = multiply_and_accumulate();
        }

        if (input_power != nullptr) {
            *input_power = rms_power(signal);
        }

        if (output_power != nullptr) {
            *output_power = rms_power(out_signal);
        }
        
        return out_signal;
    }

    /// Utility function to calculate the RMS power of a signal
    /// RMS power is computed as  \f$\sqrt{\frac{1}{N} \sum_{i=1}^{N} X_i^2}\f$
    template <typename Signal>
    static double rms_power(const Signal& signal) {
        const std::size_t num_elements = signal.size();
        if (num_elements == 0) {
            return 0.0;
        }
        double sum = 0.0;
        for (std::size_t i = 0; i < num_elements; i++) {
            sum += std::pow(signal[i], 2);
        }

        sum /= num_elements;

        return std::sqrt(sum);
    }

    /// Returns true of the filter has been initialized
    const bool is_initialized() const {
        return initialized_;
    }

    /// Returns the filter type
    const std::string& filter_type() const {
        return filter_type;
    }

    /// Resets the filter
    void reset() {
        buf_.reset();
    }
  private:
    /// Private method to perform multiply and accumulate for the convolution. 
    T multiply_and_accumulate() {
        std::fill(mac_buffer_.begin(), mac_buffer_.end(), T());
        std::size_t data_buf_size = 0;
        const auto data_buf = buf_.data(&data_buf_size);
        const bool within_range = (data_buf_size >= 0) && (data_buf_size <= num_filter_taps_);
        assert(within_range);
        assert(data_buf != nullptr);

        for (std::size_t i = 0; i < data_buf_size; i++) {
            mac_buffer_[i] = data_buf[i] * coefs_[num_filter_taps_ - data_buf_size + i];
        }

        T result = std::accumulate(mac_buffer_.begin(), mac_buffer_.end(), T());
        return result;
    }

    /// Buffer to store the intermediary outputs in the multiply and accumulate operation. 
    std::vector<T> mac_buffer_;

    /// Circular buffer which stores the data. Instance of `cb::CircularBuffer`
    /// It is of the same size as the number of filter taps.
    DataBuffer buf_;

    /// Array of filter coefficients
    FilterCoefs coefs_;

    /// Type of filter
    std::string filter_type_;

    /// Number of filter co-efficients
    std::size_t num_filter_taps_;

    /// Flag to indicate if the filter is initialized 
    bool initialized_;
};

} // namespace fir
} // namespace hid