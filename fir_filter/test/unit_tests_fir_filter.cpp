#include "fir_filter.h"


#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <vector>

namespace hid {
namespace fir {

TEST_CASE("1. Check simple uniform", "[fir_filter]") {
    constexpr std::size_t num_taps = 5;
    FIRFilter<float, num_taps> filter(FIRFilterType::kUniform);
    REQUIRE(filter.is_initialized());
    float output = 0;
    for (std::size_t i = 0; i < 2*num_taps; i++) {
        output = filter.add_data_point(1.0);
        REQUIRE(output == Approx( i < num_taps? 0.2 * (i+1) : 1.0));
    }
}


TEST_CASE("2. Check simple harmonic", "[fir_filter]") {
    constexpr std::size_t num_taps = 5;
    FIRFilter<float, num_taps> filter(FIRFilterType::kHarmonic);
    REQUIRE(filter.is_initialized());
    std::array<float, num_taps> expected_output = {5.0/15.0, 14.0/15.0, 26.0/15.0, 40.0/15.0, 55.0/15.0};
    float output = 0;
    for (std::size_t i = 0; i < num_taps; i++) {
        output = filter.add_data_point(1.0 * (i+1));
        REQUIRE(output == Approx(expected_output[i]));
    }
    output = filter.add_data_point(6.0);
    REQUIRE(output == Approx(70.0/15.0));
}

TEMPLATE_TEST_CASE("3. Check uniform filter with signal", "[fir_filter]", float, double) {
    constexpr std::size_t num_taps = 5;
    constexpr std::size_t input_length = 10;
    
    FIRFilter<TestType, input_length> u_filter(FIRFilterType::kUniform, num_taps);
    REQUIRE(u_filter.is_initialized());
    
    std::vector<TestType> input_signal(input_length, 1.0);
    auto output_signal = u_filter.filter(input_signal);


    std::vector<TestType> expected_output = 
                    {0.2, 0.4, 0.6, 0.8, 1.0 , 1.0 , 1.0 , 1.0 , 1.0 , 1.0 , 0.8, 0.6, 0.4, 0.2};
    
    REQUIRE(output_signal.size() == expected_output.size());

    for (std::size_t i = 0; i < output_signal.size(); i++) {
        REQUIRE(output_signal[i] == Approx(expected_output[i]));
    }
}


TEMPLATE_TEST_CASE("4. Check harmonic filter with signal", "[fir_filter]", float, double) {
    constexpr std::size_t num_taps = 5;
    constexpr std::size_t input_length = 10;
    
    FIRFilter<TestType, num_taps> h_filter(FIRFilterType::kHarmonic);
    REQUIRE(h_filter.is_initialized());
    
    std::vector<TestType> input_signal(input_length);
    for (std::size_t i = 1; i <= input_length; i++) {
        input_signal[i-1] = 1.0*i;
    }
    auto output_signal = h_filter.filter(input_signal);


    std::vector<TestType> expected_output = {0.33333333, 0.93333333, 1.73333333, 2.66666667, 3.66666667,
       4.66666667, 5.66666667, 6.66666667, 7.66666667, 8.66666667,
       6.        , 3.73333333, 1.93333333, 0.66666667};
    
    REQUIRE(output_signal.size() == expected_output.size());

    for (std::size_t i = 0; i < output_signal.size(); i++) {
        REQUIRE(output_signal[i] == Approx(expected_output[i]));
    }
}

} // namespace fir
} // namespace hid