
###HID Programming Exercise
The project is organized as follows -
1. All the external packages used are stored in `external`
2. The code for circular buffer and the fir filter is stored in folders with their respective names with the test sub-folder containing the unit tests. Since these two modules use templates all of the code is in the header.
3. The driver program is `main.cpp`

####Usage:

`HID` is the project root directory.
From this directory run `./waf configure` to configure the project. <br>
Then `./waf build` to build the code. <br>
The unit tests can be executed by running the binary -
`./build/<module_name>/test/unit_tests_<module_name>` where `module_name` is either `circular_buffer` or `fir_filter`.

The driver program is in `main.cpp`. This program reads the noisy signal from a `txt` file and stores the filtered signal in either the file specified by the user or `out.txt` (default option). 
This can be run with the command - 
`./build/main -i <input_file> -n <number of filter taps> -f <filter_type> (harmonic or uniform) -o <output_file> -rms <flag to display RMS power>`.

A test input file with 1000 samples of sine wave of frequency $100 Hz$ sampled at $5000 Hz$ perturbed by Gaussian noise of $\mu = 0.0$ and $\sigma = 0.2$ is present in the project folder.  

This project uses the following external/open-source modules -
i) *Catch2* for unit tests.
ii) *Cmdline* for parsing command line arguments. 


####Assumptions/Limitations:
1. This project uses `waf` as the build tool which needs python3 (< 3.7 ideally). This project is written in C++11 and has been tested only on Ubuntu 16.04 with python 3.6.5 and g++ v5.5. 

2. Since the maximum size of the `ContiguousBlockCircularBuffer` and `FIRFilter` is set at compile-time, to prevent large memory allocation on the stack a compile time check to ensure that not more than 7 MB (Catch2 framework runs into issues with > 7 MB)is allocated is included.   

3. The `ContinguousBlockCircularBuffer` is <b> not </b> thread safe.

4. The `FIRFilter` implements a full convolution i.e the output signal length will be `input_signal_length + num_filter_taps - 1`.



####Troubleshooting:
1. If `./waf configure` throws an error about `bzip2` or `data integrity`, please download a fresh copy of `waf` by executing the commands -
```
curl -o waf https://waf.io/waf-2.0.14
chmod 755 waf
# compile the entire codebase
./waf distclean configure build
```

2. If `waf` throws an error about the python version please ensure that `/usr/bin/env python` 
points to `python3`. Or the commands can be executed like so -
```
python3 waf configure
python3 waf build
```  

<b>Author</b>: Avinash Nargund, *avinash.nargund@gmail.com* 