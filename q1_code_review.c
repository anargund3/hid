char *GetErrorString( int x )
{
    char errorString[20];
    switch ( x )
    {
        case 0:
            // This assignment is invalid. The compiler will throw an error.
            // String copy has to be used.  
            errorString = "Success -- No error."; 
            break;
        case 2:
            // This assignment is invalid. The compiler will throw an error  
            // String copy has to be used.
            errorString = "Overflow!";
        break;
        // Having a default case to handle unexpected `err` values would be ideal.
    }
    // Assuming that this is done to null terminate the string, this is unecessary
    errorString[19] = 0;

    // Returning an address of a local variable will result in compilation error or 
    // junk values if the warning is suppressed .
    return errorString;
}

void main( void )
{
    int err = DoSomething();

        
    // Logical error - The `GetErrorString` function is called only when x != 0 so even if the
    // compilation errors are fixed `case 0` in that function will never be executed.
    if ( err )
    {
        printf( "%s\n", GetErrorString( err ) );
    }
}