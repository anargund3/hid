#include "cmdline/cmdline.h"
#include "fir_filter.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

/// Driver script to test the FIR filter. 
/// Can pass the signal through a file and obtain the filtered signal in another file.
/// Usage - ./build/main -i <input_file> -f <uniform, harmonic> -n <num_taps> -o <output_file> -rms
///         ./build/main -h prints the help
using namespace hid;

int main(int arc, char *argv[]) {
    constexpr std::size_t max_num_taps = 100;
    cmdline::parser arg_parser;
    
    arg_parser.add<std::string>("input_file", 'i', 
    "input file containing the signal to be filtered", false, "test_signal.txt");

    arg_parser.add<std::string>("filter_type", 'f', "type of filter", false, "uniform",
                                cmdline::oneof<std::string>("uniform", "harmonic"));

    arg_parser.add<std::size_t>("num_taps", 'n', "number of filter taps", false, 8, 
                                cmdline::range(1,100));

    arg_parser.add<std::string>("output_file", 'o', 
                        "file into which the output is written into", false, "out.txt");

    arg_parser.add("rms", '\0', "display input and output rms power");

    const bool args_ok = arg_parser.parse(arc, argv);

    if (!args_ok) {
        std::cout << arg_parser.error() << std::endl << arg_parser.usage() << std::endl;
        return 1;
    }

    const std::string in_filename = arg_parser.get<std::string>("input_file");

    const std::string filter_type_str = arg_parser.get<std::string>("filter_type");

    const std::size_t num_taps = arg_parser.get<std::size_t>("num_taps");

    fir::FIRFilterType filter_type = filter_type_str == "uniform"? \
                fir::FIRFilterType::kUniform : fir::FIRFilterType::kHarmonic;

    bool display_power = arg_parser.exist("rms");

    const std::string out_filename = arg_parser.get<std::string>("output_file");

    std::fstream in_file(in_filename, std::ios_base::in);
    if (in_file.fail()) {
        std::cout << "Input file not found. Exiting...\n";
        return 1;
    }
    
    std::fstream out_file(out_filename, std::ios_base::out);
    if (out_file.fail()) {
        std::cout << "Unable to open output file. Exiting...\n";
        return 1;
    } 
    
    std::vector<double> read_signal{};
    std::string line;

    while (std::getline(in_file, line)) {
        read_signal.push_back(std::stod(line));
    }

    std::cout << read_signal[0] << std::endl;
    if (read_signal.empty()) {
        std::cout << "Could not read the input signal, possibly empty input file. Exiting...\n";
        return 1;
    }

    fir::FIRFilter<double, max_num_taps> fir_filter(filter_type, num_taps);

    double in_power = 0.0, out_power = 0.0;

    auto out_signal = fir_filter.filter(read_signal, &in_power, &out_power);

    if (display_power) {
        std::cout << "Input RMS power " << in_power << std::endl;
        std::cout << "Output RMS power " << out_power << std::endl;
    }

    for (const auto value : out_signal) {
        out_file << std::to_string(value) << std::endl;
    }

    return 0;
}