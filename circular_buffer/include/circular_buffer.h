#pragma once

#include <cassert>
#include <cstddef>
#include <limits>

namespace hid {
namespace cb {
/// Circular buffer with max size set at compile time.
/// Allocates almost twice the memory to facilitate O(1) linear buffer retrieval. 
template <typename T, std::size_t N>
class ContiguousBlockCircularBuffer {
  static_assert(N > 0, "Size cannot be zero");
  public:
    ContiguousBlockCircularBuffer() = default;

    ContiguousBlockCircularBuffer(const std::size_t size) {
        assert(size > 0);
        assert(size <= N);
        max_size_ = size;
        head_ = 0;
        tail_ = 0;
        aux_head_ = max_size_;
        full_ = false;
        
    } 

    /// Inserts element into the circular buffer.
    /// Overwrites the oldest element if the buffer is full
    void push(const T& item) {
        data_[head_] = item;
        
        if (full_) {
            tail_ = (tail_ + 1) % max_size_;
        }

        if (head_ < tail_) {
            data_[aux_head_] = item;
            aux_head_ = (aux_head_+1) % (2 * max_size_ - 1);
            if (aux_head_ < max_size_) {
                aux_head_ += max_size_;
            }
        }

        head_ = (head_ + 1) % max_size_;
        full_ = (head_ == tail_);
    }

    /// Removes the oldest element in the queue.
    /// Returns true if the operation was sucessful.
    bool pop() {
        if (empty()) {
            return false;
        }
        tail_ = (tail_ + 1) % max_size_;
        full_ = false;
        return true;
    }

    /// Returns a mutable reference to the front of the buffer.
    /// Results in undefined behavior if the buffer is empty.
    T& front() {
        return data_[tail_];
    }

    /// Returns a constant reference to the front of the buffer.
    /// Results in undefined behavior if the buffer is empty.
    const T& front() const {
        return data_[tail_];
    }
    /// Returns true if the circular buffer is empty.
    bool empty() const {
        return (!full_ && head_ == tail_);
    }

    /// Returns true if the circular buffer is full.
    bool full() const {
        return full_;
    }

    /// Returns the capacity of the buffer.
    std::size_t max_size() const {
        return max_size_;
    }

    /// Returns the number of elements in the buffer
    std::size_t size() const {
        if (empty()) {
            return 0;
        } else if (full())  {
            return max_size_;
        }
        else if (tail_ < head_) {
            return (head_ - tail_);
        } else {
            return (aux_head_ - tail_);
        }
    }


    /// Return data as linear buffer.
    /// Takes in the size by reference and updates it.
    /// Reading the buffer beyond the size results in undefined behavior.
    const T* data(std::size_t* size) const {
        assert(size != nullptr);
        *size = this->size();
        return data_ + tail_;
    }

    /// Resets the filter. Does not delete the contents.
    void reset() {
        head_ = 0;
        tail_ = 0;
        aux_head_ = max_size_;
        full_ = false;
    }

    ~ContiguousBlockCircularBuffer() = default;

  private:
    /// Capacity of the circular buffer set at compile time
    std::size_t max_size_ = N;
    
    /// Index at which a new element will be inserted into the buffer
    std::size_t head_ = 0;
    
    /// Index of the oldest element in the circular buffer
    std::size_t tail_ = 0;

    /// Auxiliary head used to return a linear buffer
    std::size_t aux_head_ = N;

    /// Indicates if the buffer is full 
    bool full_ = false;

    /// Allocate twice the max size to allow `get` to be an O(1) operation
    // TODO: Check if std::array would be better?
    T data_[2*N - 1];
}; 
template <std::size_t N>
using IntCircularBuffer = ContiguousBlockCircularBuffer<int, N>;

template <std::size_t N>
using FloatCircularBuffer = ContiguousBlockCircularBuffer<float, N>;

} // namespace cb
} // namespace hid