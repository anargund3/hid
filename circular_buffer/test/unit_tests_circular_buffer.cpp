#include "circular_buffer.h"

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>


namespace hid {
namespace cb {
// helper function
/// Will fill the buffer with increasing values from [start, end]
template <typename T, std::size_t N>
void fill_buffer_linear(ContiguousBlockCircularBuffer<T, N>* cbuf, std::size_t start = 1, std::size_t end = N) {
    assert(cbuf != nullptr);
    for (std::size_t  i = start; i <= end; i++) {
        cbuf->push(i);
    }
}

TEST_CASE("1: Test push, front and pop", "[circular_buffer]") {
    constexpr std::size_t size = 6;
    IntCircularBuffer<size> cbuf;

    // try to pop from empty list
    REQUIRE_FALSE(cbuf.pop());

    fill_buffer_linear(&cbuf);

    for (std::size_t i = 1; i <= size; i++) {
        REQUIRE(cbuf.front() == i);
        REQUIRE(cbuf.pop());
    }

    // try to pop from empty list
    REQUIRE_FALSE(cbuf.pop());
}

TEST_CASE("2: Test full and empty", "[circular_buffer]") {

    constexpr std::size_t size = 5;
    IntCircularBuffer<size> cbuf;

    REQUIRE_FALSE(cbuf.full());
    REQUIRE(cbuf.empty());

    // push 4 elements into the buffer
    fill_buffer_linear(&cbuf, 1, size-1);

    REQUIRE_FALSE(cbuf.full());
    REQUIRE_FALSE(cbuf.empty());

    cbuf.push(size);

    REQUIRE(cbuf.full());
    REQUIRE_FALSE(cbuf.empty());

    cbuf.reset();

    REQUIRE_FALSE(cbuf.full());
    REQUIRE(cbuf.empty());


}

TEST_CASE("3: Test linear buffer getter",  "[circular_buffer]") {

    constexpr std::size_t size = 6;
    IntCircularBuffer<size> cbuf;

    std::size_t buf_size = 0;
    const int* data = nullptr;

    data = cbuf.data(&buf_size);
    REQUIRE(buf_size == 0);
    
    fill_buffer_linear(&cbuf);

    // The buffer is now [1, 2, 3, 4, 5, 6]
    data = cbuf.data(&buf_size);

    REQUIRE(buf_size == size);
    REQUIRE(data != nullptr);

    for (std::size_t i = 1; i <= buf_size; i++) {
        REQUIRE(i == data[i-1]);
    }

    fill_buffer_linear(&cbuf, 7, 8);

    // The buffer is now [7,8,3,4,5,6]
    data = cbuf.data(&buf_size);

    REQUIRE(buf_size == size);
    REQUIRE(data != nullptr);

    for (std::size_t i = 1; i <= buf_size; i++) {
        REQUIRE(i+2 == data[i-1]);
    }

    for (std::size_t i = 0; i < 3; i++) { 
        REQUIRE(cbuf.pop());
    }

    // The buffer is now [7,8, _, _, _, 6]
    data = cbuf.data(&buf_size);

    REQUIRE(buf_size == 3u);
    REQUIRE(data != nullptr);

    for (std::size_t i = 0; i < buf_size; i++) {
        REQUIRE(i+6 == data[i]);
    }

    fill_buffer_linear(&cbuf, 9, 11);

    // The buffer is now [7,8,9,10,11,6]
    data = cbuf.data(&buf_size);

    REQUIRE(buf_size == size);
    REQUIRE(data != nullptr);

    for (std::size_t i = 0; i < buf_size; i++) {
        REQUIRE(i+6 == data[i]);
    }

    fill_buffer_linear(&cbuf, 12, 24);

    // The buffer is now [19, 20, 21, 22, 23, 24]
    data = cbuf.data(&buf_size);
    REQUIRE(buf_size == size);
    REQUIRE(data != nullptr);

    for (std::size_t i = 0; i < buf_size; i++) {
        REQUIRE(i+19 == data[i]);
    }
    
    cbuf.reset();

    data = cbuf.data(&buf_size);
    REQUIRE(buf_size == 0);
}

TEST_CASE("4. Test reset", "[circular_buffer]") {

    constexpr std::size_t size = 6;
    IntCircularBuffer<size> cbuf;
    
    fill_buffer_linear(&cbuf);

    REQUIRE_FALSE(cbuf.empty());

    cbuf.reset();

    REQUIRE(cbuf.empty());

    fill_buffer_linear(&cbuf, 10, 13);
    REQUIRE(cbuf.front() == 10);
    REQUIRE(cbuf.size() == 4);
}

TEST_CASE("5: Test large size", "[fir_filter]") {
    
    constexpr std::size_t size = 100000;
    IntCircularBuffer<size> cbuf;

    // try to pop from empty list
    REQUIRE_FALSE(cbuf.pop());

    fill_buffer_linear(&cbuf);

    for (std::size_t i = 1; i <= size; i++) {
        REQUIRE(cbuf.front() == i);
        REQUIRE(cbuf.pop());
    }

    // try to pop from empty list
    REQUIRE_FALSE(cbuf.pop());
}

TEST_CASE("6: Test max size", "[fir_filter]") {
    constexpr std::size_t max_size = 10;
    const std::size_t size = 5;
    IntCircularBuffer<max_size> cbuf(size);

    fill_buffer_linear(&cbuf, 1, size);

    REQUIRE(cbuf.full());
    REQUIRE(cbuf.size() == size);
    REQUIRE(cbuf.front() == 1);

}


} // namespace cb
} // namespace hid