#! /usr/bin/env python3
# encoding: utf-8

import os

top = '.'
out = 'build'

def options(opt):
    opt.load('compiler_cxx waf_unit_test')

def configure(conf):
    conf.load('compiler_cxx waf_unit_test')
    conf.env.append_value('CXXFLAGS', ['-std=c++11', '-g', '-Wall', '-Werror', '-Wframe-larger-than=7340032', '-O3'])
    try:
       # cannot upload external folder to git, hence this
       conf.recurse('external')
    except:
        pass

def build(bld):
    dirs = next(os.walk(top))[1]
    for d in dirs:
        if d == 'build' or d[0] == '.':
            continue 
        try:
            bld.recurse(d)
        except:
            pass

    bld.program(source ='main.cpp',
                target = 'main',
                use = ['fir_filter', '3rdparty_cmd'])
